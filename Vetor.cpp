#include <iostream>
#include <iomanip>
#include <cctype> //Captura Tecla

using namespace std;

int main ()
{
	float md[8]; /* Definindo o tamanho do vetor */
	float soma,media;
	int linha;
	char letra;
	
	// Montando a formata��o da entrada e sa�da de dados
	
	cout << setprecision(2);
	cout << setiosflags (ios::right);
	cout << setiosflags (ios::fixed);
	cout << "Calculo de media escolar\n\n";
	
	// Preparando a entrada de dados
	
	for (linha = 0; linha <= 7; linha++)
	{
		cout <<"Informe a " <<linha + 1 <<"a. Nota: ";
		
		// Gerando o acomulado de media
		
		cin >> md [linha];
		soma += md[linha];
	}
	
	// Calculando a m�dia acumulada na soma
	
	media = soma/8;
	
	// Preparando a sa�da de dados
	
	cout <<"\nA media do grupo equivalente a: " <<setw(8);
	cout <<media <<endl;
	
	// Contruindo o controle de sa�da do programa
	
	cout <<"\nTecle <F> + <Enter> para finalizar o programa";
	cout <<endl;
	
	do
		{
			letra = cin.get();
			letra = toupper(letra);
		}
	while (letra != 'F');
	
}
