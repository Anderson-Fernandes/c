#include  <iostream>
#include <conio.h>
#include <stdio.h>
#include <iomanip>

using namespace std;

int main ()

{
	float pe,cp,pdca,pcca,p2,p3,p2d,p3d;
	cout <<"Entre com o preco: " <<endl;
	cout <<"RS";
	cin >>pe;
	pdca=pe*10/100;
	pdca=pe-pdca;
	pcca=pe*5/100;
	pcca=pe-pcca;
	p2=pe;
	p3=pe*10/100;
	p3=pe+pe;
	p2d=p2/2;
	p3d=p3/3;
	cout <<"\tEscolha o modo de pagamento: " <<endl;
	cout <<"[1] A vista em dinheiro ou cheque = RS" <<pdca <<endl;
	cout <<"[2] A vista com cartao de credito = RS" <<pcca <<endl;
	cout <<"[3] Em 2 vezes = RS" ;
	cout <<setiosflags (ios::fixed) <<setprecision (2) <<p2d;
	cout <<" (Total: RS" <<p2 <<")" <<endl;
	cout <<"[4] Em 3 vezes = RS" <<p3d <<" (Total: RS" <<p3 <<")" <<endl;
	cout <<"------------------------------------------------------------------------------" <<endl;
	cin >>cp;
	if (cp == 1)
	{
		cout <<"Opcao escolhida com sucesso" <<endl;
		cout <<"O Pagamento sera feito em dinheiro ou cheque com o preco de:RS " <<pdca;
	}
	else
	{
		if (cp == 2)
		{
			cout <<"Opcao escolhida com sucesso" <<endl;
			cout <<"O pagamento sera feito com o cartao de credito com o preco de:RS " <<pcca;
		}
		else
		{
			if (cp == 3)
			{
				cout <<"Opcao escolhida com sucesso" <<endl;
				cout <<"O pagamento sera feito em 2x com o preco de: RS";
				cout <<setiosflags (ios::fixed) <<p2d; 
				cout <<" (Total de RS" <<p2 <<")";
			}
			else
			{
				if (cp == 4)
				{
					cout <<"Opcao escolhida com sucesso" <<endl;
					cout <<"O pagamento sera feito em 3x com o preco de: RS";
					cout <<setiosflags (ios::fixed) <<p3d;
					cout <<" (Total de RS" <<p3 <<")";
 				}
 				else
 				{
 					cout <<"Opcao invalida, por favor utilizar o menu";
 				}
			}
		}
	}
	getch ();
}
