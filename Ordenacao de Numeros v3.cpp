#include <iostream>
#include <iomanip>
#include <cctype>

using namespace std;

int main ()
{
	int a[5], i, j, x;
	char LETRA;
	cout <<"Leitura de apresentacao ordenada de valores\n\n";
	
	//Entrada de Valores
	
	for (i=0; i<=4; i++)
	{
		cout <<"Informe o" <<setw (2) << i + 1 <<"o. valor = ";
		cin >> a[i];
	}
	cout <<endl;
	
	//Classificação dos Valores
	
	for (i=0; i<=3; i++)
		for (j=i+1; j<=4; j++)
			if(a[i] > a[j])
			{
				x = a[i];
				a[i] = a[j];
				a[j] = x;
			}
			
	//Apresentação de Valores
	
	for (i=0; i<=4; i++)
	{
		cout << setw(2) << i + 1 <<"o. valor = " << a[i] <<endl;
	}
	cout << endl;
	cout <<"\nTecle <F> + <Enter> para finalizar o programa";
	do
	{
		LETRA = cin.get();
		LETRA = toupper (LETRA);
	}
	while (LETRA != 'F');
}
