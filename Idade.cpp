#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <time.h>

using namespace std;

int main()

{
	int dn,dh,mn,mh,an,ah,idd,idm,ida;
	char sn;
	time_t currentTime;
	time (&currentTime);
	struct tm *myTime = localtime (&currentTime);
	dh = (myTime ->tm_mday);
	mh = (myTime ->tm_mon + 1);
	ah = (myTime ->tm_year + 1900);
	cout << "\t\t\t\tNascimento" <<endl;
	cout << "------------------------------------------------------------------------------" <<endl;
	cout <<"Dia: ";
	cin >> dn;
	cout <<"Mes: ";
	cin >>mn;
	cout <<"Ano: ";
	cin >>an;
	if (an >= 1000 && an <=9999)
	{
		if (mn >=1 && mn <=12)
		{
			if (mn == 1 || mn == 3 || mn == 5 || mn == 7 || mn == 8 || mn == 10 ||mn == 12)
			{
				if (dn <=31 && dn > 0)
				{
					cout <<"------------------------------------------------------------------------------" <<endl;
				}
				else
				{
					cout <<"Dia Invalido";
					return (0);
				}
			}
			else
			{
				if (mn == 4 || mn == 6 || mn == 9 || mn == 11)
				{
					if (dn <=30 && dn >0)
					{
						cout <<"------------------------------------------------------------------------------" <<endl;
					}
					else
					{
						cout <<"Dia Invalido";
						return (0);
					}
				}
				else
				{
					if (mn == 2)
					{
						if (an %4 == 0 && (an % 400 == 0 || an % 100 !=0))
						{
							if (dn <= 29 || dn > 0)
							{
								cout <<"------------------------------------------------------------------------------" <<endl;
							}
							else
							{
								if (dn <= 28 || dn >0)
								{
									cout <<"------------------------------------------------------------------------------" <<endl;
								}
								else
								{
									cout <<"Dia Invalido";
									return (0);
								}
							}
						}
					}
				}
			}
		}
		else
		{
			cout <<"Mes Invalido";
			return (0);
		}
	}
	else
	{
		cout <<"Ano invalido";
		return (0);
	}
	cout <<endl;
	cout <<"\t\t\t\tData Atual" <<endl;
	cout <<"------------------------------------------------------------------------------" <<endl;
	cout <<"Dia: " <<dh <<endl;
	cout <<"Mes: " <<mh <<endl;
	cout <<"Ano: " <<ah <<endl;
	cout <<"------------------------------------------------------------------------------" <<endl;
	cout <<"Dia Atual Correto? (S para sim | N para nao)" <<endl;
	cin >>sn;
	cout <<endl;
	if (sn == 's' || sn == 'n')
	{
		if(sn == 'n')
		{
			cout <<"Digite o dia atual: ";
			cin >>dh;
			cout <<"Digite o mes atual: ";
			cin >>mh;
			cout <<"Digite o ano atual: ";
			cin >>ah;
		}
	}
	else
	{
		cout <<"Resposta invalida";
	}
		if (ah >= 1000 && ah <=9999)
	{
		if (mh >=1 && mh <=12)
		{
			if (mh == 1 || mh == 3 || mh == 5 || mh == 7 || mh == 8 || mh == 10 ||mh == 12)
			{
				if (dh <=31 && dh > 0)
				{
					cout <<"------------------------------------------------------------------------------" <<endl;
				}
				else
				{
					cout <<"Dia Invalido";
					return (0);
				}
			}
			else
			{
				if (mh == 4 || mh == 6 || mh == 9 || mh == 11)
				{
					if (dh <=30 && dn >0)
					{
						cout <<"------------------------------------------------------------------------------" <<endl;
					}
					else
					{
						cout <<"Dia Invalido";
						return (0);
					}
				}
				else
				{
					if (mh == 2)
					{
						if (ah %4 == 0 && (ah % 400 == 0 || ah % 100 !=0))
						{
							if (dh <= 29 || dh > 0)
							{
								cout <<"------------------------------------------------------------------------------" <<endl;
							}
							else
							{
								if (dh <= 28 || dh >0)
								{
									cout <<"------------------------------------------------------------------------------" <<endl;
								}
								else
								{
									cout <<"Dia Invalido";
									return (0);
								}
							}
						}
					}
				}
			}
		}
		else
		{
			cout <<"Mes Invalido";
			return (0);
		}
	}
	else
	{
		cout <<"Ano invalido";
		return (0);
	}
	idd= dh - dn;
	idm= mh - mn;
	ida= ah - an;
	if (idm <=0)
	{
		idm = mn - mh; 
		ida = ida -1;
	}
	if (idd <=0)
	{
		idd=dn - dh;
		idm= idm -1;
		if (idm <=0)
		{
			idm = 12-idm;
			ida = ida-1;
		}
		if (idm == 1 || idm == 3 || idm == 5 || idm ==7 || idm == 8 || idm == 10 || idm == 12)
		{
			idd = 31 - idd;
		}
		else
		{
			if (idm == 4 || idm == 6 || idm == 9 || idm == 11)
			{
				idd = 30 - idd;
			}
			else
			{
				if (ah %4 == 0 && (ah % 400 == 0 || ah % 100 !=0))
				{
					idd = 29 - idd;
				}
				else
				{
					idd = 28 - idd;
				}
			}
		}
	}
	cout <<"Sua idade REAL e:" <<endl;
	cout <<ida <<"ano(s) " <<idm <<"Mes(es) e " <<idd <<"Dia(s)" ;
	getch ();
}
